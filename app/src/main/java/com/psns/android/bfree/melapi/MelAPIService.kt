package com.psns.android.bfree.melapi

import com.psns.android.bfree.melapi.data.Response
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MelAPIService {
    @GET("?")
    fun fetch(
        @Query("dataset") dataset: String? = null,
        @Query("q") q: String? = null,
        @Query("lang") lang: String? = null,
        @Query("rows") rows: Int? = 10,
        @Query("start") start: Int? = null,
        @Query("sort") sort: List<String>? = null,
        @Query("facet") facet: List<String>? = null,
        @Query("refine") refine: Map<String, String>? = null,
        @Query("exclude") exclude: Map<String, String>? = null,
        @Query("geofilter.distance") geofilterDistance: List<String>? = null,
        @Query("geofilter.polygon") geofilterPolygon: String? = null,
        @Query("timezone") timezone: String? = null,
        @Query("fields") fields: List<String>? = null
    ): Call<Response>
}