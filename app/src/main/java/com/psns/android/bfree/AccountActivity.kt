package com.psns.android.bfree

import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.psns.android.bfree.account.AccountView
import com.psns.android.bfree.account.CardAdapter
import com.psns.android.bfree.account.CardModal
import com.psns.android.bfree.db.AppDatabase
import com.psns.android.bfree.db.Card

private val TAG = AccountActivity::class.java.simpleName

class AccountActivity : AppCompatActivity(), CardAdapter.OnItemClickListener {

    private lateinit var cards: List<Card>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(TAG, "onCreate")
        setContentView(R.layout.activity_account)
        ViewModelProvider(this)[AccountView::class.java]
        val db = AppDatabase.getDatabase(this)

        // init view model
        val button = findViewById<Button>(R.id.buttonAdd)
        cards = db.cardDao().getAll()
        Log.d(TAG, "cards: $cards")

        val recyclerView = findViewById<RecyclerView>(R.id.cardListLayout)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = CardAdapter(db, cards as ArrayList<Card>, this)

        button.setOnClickListener {
            val card = CardModal(db, null)
            card.show(supportFragmentManager, "BottomCardModal")
        }

    }

    override fun onItemClick(position: Int) {
        Log.d(TAG, "onItemClick: $position")
        CardModal(AppDatabase.getDatabase(this), cards[position]).show(supportFragmentManager, "BottomCardModal")
    }

}