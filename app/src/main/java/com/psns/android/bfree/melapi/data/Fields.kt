package com.psns.android.bfree.melapi.data

import com.google.gson.annotations.SerializedName

data class Fields(
    @SerializedName("nbvelosdispo")
    val nbvelosdispo: Int? = null,
    @SerializedName("nbplacesdispo")
    val nbplacesdispo: Int? = null,
    @SerializedName("libelle")
    val libelle: Any? = null,
    @SerializedName("adresse")
    val adresse: String? = null,
    @SerializedName("nom")
    val nom: String? = null,
    @SerializedName("etat")
    val etat: String? = null,
    @SerializedName("commune")
    val commune: String? = null,
    @SerializedName("etatconnexion")
    val etatConnexion: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("geo")
    val geo: List<Float>? = null,
    @SerializedName("localisation")
    val localisation: List<Float>? = null,
    @SerializedName("datemiseajour")
    val datemiseajour: String? = null,
    @SerializedName("prix")
    val prix: Float?,
    @SerializedName("typetitreproduit")
    val typeTitre: String?
)
