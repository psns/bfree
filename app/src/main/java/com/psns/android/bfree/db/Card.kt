package com.psns.android.bfree.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.psns.android.bfree.melapi.Subscription
import java.time.LocalDateTime

@Entity
data class Card(
    var id: String,
    var type: Subscription,
    var range: String,
    var validity: String,
) {
    @PrimaryKey(autoGenerate = true)
    var cardId: Int = 0
}
