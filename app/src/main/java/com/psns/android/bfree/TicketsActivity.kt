package com.psns.android.bfree

import android.os.Bundle
import android.util.Log
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.psns.android.bfree.melapi.Subscription
import com.psns.android.bfree.tickets.TicketAdapter
import com.psns.android.bfree.tickets.TicketsViewModel

private val TAG = TicketsActivity::class.java.simpleName

class TicketsActivity : AppCompatActivity() {

    private lateinit var viewModel: TicketsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tickets)

        viewModel = ViewModelProvider(this)[TicketsViewModel::class.java]
        val radioGroup = findViewById<RadioGroup>(R.id.radioTickets)

        viewModel.getType().observe(this) {
            val tickets = viewModel.getTickets()

            Log.d(TAG, tickets.toString())

            if (tickets == null || tickets.isEmpty()) {
                return@observe
            }

            val recyclerView = findViewById<RecyclerView>(R.id.ticketsListLayout)
            recyclerView.layoutManager = LinearLayoutManager(this)

            val adapter = TicketAdapter(tickets)
            recyclerView.adapter = adapter
        }

        radioGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.radioVlille -> viewModel.setType(Subscription.VLILLE.toString())
                R.id.radioSub -> viewModel.setType(Subscription.ABONNEMENT.toString())
                R.id.radioCasual -> viewModel.setType(Subscription.OCASSIONNELS.toString())
                R.id.radioSupport -> viewModel.setType(Subscription.SUPPORT.toString())
            }
        }
    }
}