package com.psns.android.bfree.account

import android.app.DatePickerDialog
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.psns.android.bfree.R
import com.psns.android.bfree.db.AppDatabase
import com.psns.android.bfree.db.Card
import com.psns.android.bfree.melapi.Subscription
import java.time.LocalDate
import java.time.OffsetDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*


private val TAG = CardModal::class.java.simpleName

class CardModal(private val db: AppDatabase, private val card: Card?) : DialogFragment() {

    private var cal = Calendar.getInstance()

    private lateinit var accountViewModel: AccountView

    private lateinit var idView: TextView
    private lateinit var typeView: Spinner
    private lateinit var rangeView: Spinner
    private lateinit var validityView: TextView

    private var isVlilleSelected = false
    private val dateFormat = "yyyy/MM/dd"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        Log.d(TAG, "onCreateView")
        val view = inflater.inflate(R.layout.modal_bottom_card, container, false)
        accountViewModel = ViewModelProvider(activity!!)[AccountView::class.java]

        val resetButton = view.findViewById<Button>(R.id.buttonAdd)
        val saveButton = view.findViewById<Button>(R.id.buttonSave)
        val image = view.findViewById<ImageView>(R.id.ic_card)

        idView = view.findViewById<EditText>(R.id.idValue)
        typeView = view.findViewById(R.id.typeValue)
        rangeView = view.findViewById(R.id.rangeValue)
        validityView = view.findViewById<EditText>(R.id.validityValue)

        typeView.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, Subscription.values())

        if (card != null) {
            Log.d(TAG, "card : $card")

            idView.text = card.id
            typeView.setSelection(card.type.ordinal)

            val date = OffsetDateTime.parse(card.validity, DateTimeFormatter.ISO_DATE_TIME)
            Log.d(TAG, "date : ${date.format(DateTimeFormatter.ofPattern(dateFormat))}")

            validityView.text = date.format(DateTimeFormatter.ofPattern(dateFormat))

            updateDateInView()
        } else {

            idView.text = accountViewModel.getId()
            typeView.setSelection(accountViewModel.getType()?.ordinal ?: 0)
            rangeView.setSelection(accountViewModel.getRange() ?: 0)
            validityView.text = accountViewModel.getValidity() ?: resources.getText(R.string.date_format)
        }

        // create an OnDateSetListener
        val dateSetListener = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateDateInView()
        }

        typeView.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // do nothing
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val sub = typeView.selectedItem as Subscription
                image.setImageResource(when (sub) {
                    Subscription.VLILLE -> R.drawable.ic_vlille
                    else -> R.drawable.ic_pass_pass_card
                })

                val range =
                    accountViewModel.getSubscriptions()?.filter { it?.typeTitre == sub.toString() }?.map { it?.libelle }
                rangeView.adapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item, range!!)


                if (card != null) {
                    rangeView.setSelection(range.indexOf(card.range))
                } else if (accountViewModel.getRange() != null) {
                    rangeView.setSelection(accountViewModel.getRange() ?: 0)
                } else {
                    rangeView.setSelection(0)
                }
            }
        }

        rangeView.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // do nothing
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val range = rangeView.selectedItem as String

                if (range.contains("V'lille")) {
                    isVlilleSelected = true
                    var day = range.substring("V'lille".length + 1, "V'lille".length + 2)
                    day = if (day.isDigitsOnly()) day else "1" // default to 1

                    val date = LocalDate.now().plusDays(Integer.parseInt(day).toLong())
                    cal.set(Calendar.YEAR, date.year)
                    cal.set(Calendar.MONTH, date.monthValue - 1)
                    cal.set(Calendar.DAY_OF_MONTH, date.dayOfMonth)
                    updateDateInView()
                } else if (card?.type == Subscription.VLILLE || card == null) {
                    isVlilleSelected = false
                    validityView.text = resources.getText(R.string.date_format)
                }
            }
        }

        // when you click on the text, show DatePickerDialog that is set with OnDateSetListener
        validityView.setOnClickListener {
            if (!isVlilleSelected) DatePickerDialog(context!!, dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH)).show()
        }

        resetButton.setOnClickListener {
            idView.text = ""
            validityView.text = resources.getText(R.string.date_format)
        }

        saveButton.setOnClickListener {
            Log.d(TAG, "onCreateView: button clicked")

            val id = idView.text.toString()
            val type = typeView.selectedItem as Subscription
            val range = rangeView.selectedItem as String
            val validity = cal.time.toInstant().atZone(ZoneId.systemDefault()).toOffsetDateTime().toString()

            if (id.isNotEmpty() && id.isDigitsOnly() && validity.isNotEmpty()) {

                val card = Card(id, type, range, validity)
                Log.d(TAG, "ButtonClick: card: $card")
                if (this.card != null) db.cardDao().delete(this.card)

                db.cardDao().insert(card)
                activity?.finish()
                activity?.overridePendingTransition(0, 0);
                startActivity(activity?.intent)
                dismiss()
            } else {
                view.findViewById<TextView>(R.id.errorText).visibility = View.VISIBLE
                Handler(Looper.getMainLooper()).postDelayed({
                    view.findViewById<TextView>(R.id.errorText).visibility = View.INVISIBLE
                }, 2000)
            }
        }

        return view
    }

    /**
     * Updates the date in the TextView with correct format
     */
    private fun updateDateInView() {
        val dateFormat = "yyyy/MM/dd"
        validityView.text = cal.time.toInstant().atZone(ZoneId.systemDefault()).toOffsetDateTime()
            .format(DateTimeFormatter.ofPattern(dateFormat))
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy")

        accountViewModel.setId(idView.text.toString())
        accountViewModel.setType(typeView.selectedItem as Subscription)
        accountViewModel.setRange(rangeView.selectedItemPosition)
        accountViewModel.setValidity(cal.time.toInstant().toString())

        Log.d(TAG, "onDestroy: accountViewModel: $accountViewModel")

        super.onDestroy()
    }

}
