package com.psns.android.bfree.melapi

import java.util.*

enum class Subscription(private val value: String) {
    VLILLE("Vlille"),
    ABONNEMENT("Abonnement"),
    OCASSIONNELS("occasionnels"),
    SUPPORT("support");

    override fun toString(): String {
        // First letter is capitalized
        return value
    }

}