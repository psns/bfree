package com.psns.android.bfree.melapi.data

import com.google.gson.annotations.SerializedName

data class Facet(
    @SerializedName("name")
    val name: String,
    @SerializedName("count")
    val count: Int,
    @SerializedName("state")
    val state: String,
    @SerializedName("path")
    val path: String
)
