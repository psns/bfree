package com.psns.android.bfree.melapi.data

import com.google.gson.annotations.SerializedName

data class FacetGroup(
    @SerializedName("name")
    val name: String,
    @SerializedName("facets")
    val facets: List<Facet>
)
