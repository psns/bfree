package com.psns.android.bfree.melapi

// Defines lang parameter
const val LANG_FR = "fr"

// Defines sortable fields
const val SORT_NBVELODISPO = "nbvelosdispo"
const val SORT_NBPLACESDISPO = "nbplacesdispo"

// Defines facet possible fields
const val FACET_ETAT = "etat"
const val FACET_COMMUNE = "commune"
const val FACET_TYPE = "type"

// Defines available dataset
const val DATASET_VLILLE = "vlille-realtime"
const val DATASET_PRICES = "ilevia-gammetarifaire"
