package com.psns.android.bfree.map

import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.model.LatLng

object MapConfig {
    private const val FASTEST_INTERVAL: Long = 1000 // 1 sec
    const val REFRESH_TIME: Long = FASTEST_INTERVAL * 60 // 1 minute
    val defaultLocation = LatLng(0.0, 0.0)

    var mLocationRequest = LocationRequest.create().apply {
        interval = FASTEST_INTERVAL
        fastestInterval = 500
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        maxWaitTime = 10000
        smallestDisplacement = 50f // 50 meters
    }


    const val DEFAULT_ZOOM = 16f
    const val MAX_ZOOM = 20f
    const val MIN_ZOOM = 10f
}