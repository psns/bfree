package com.psns.android.bfree.melapi.data

import com.google.gson.annotations.SerializedName

data class Response (
    @SerializedName("nhits")
    val nhits: Int = 0,

    @SerializedName("parameters")
    val parameters: Parameters? = null,

    @SerializedName("records")
    val records: List<Record>? = null,

    @SerializedName("facet_groups")
    val facetGroups: List<FacetGroup>? = null
)
