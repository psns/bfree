package com.psns.android.bfree

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class BottomNav : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_bottom_nav, container, false)

        val navView = view.findViewById(R.id.navigation) as BottomNavigationView

        when (activity) {
            is MainActivity -> navView.selectedItemId = R.id.nav_maps
            is AccountActivity -> navView.selectedItemId = R.id.nav_account
            else -> navView.selectedItemId = R.id.nav_tickets
        }

        navView.setOnItemSelectedListener {
            Log.d("BottomNav", "navView id: ${it.tooltipText}")
            when (it.itemId) {
                R.id.nav_maps -> if (activity !is MainActivity) {
                    activity?.finish()
                    activity?.overridePendingTransition(0, 0);
                    startActivity(Intent(activity, MainActivity::class.java))

                }
                R.id.nav_account -> if (activity !is AccountActivity) {
                    activity?.finish()
                    activity?.overridePendingTransition(0, 0);
                    startActivity(Intent(activity, AccountActivity::class.java))
                }
                else -> if (activity !is TicketsActivity) {
                    activity?.finish()
                    activity?.overridePendingTransition(0, 0);
                    startActivity(Intent(activity, TicketsActivity::class.java))
                }
            }
            false
        }

        return view
    }

}