package com.psns.android.bfree.tickets

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.psns.android.bfree.R
import com.psns.android.bfree.melapi.Subscription
import com.psns.android.bfree.melapi.data.Fields


private val TAG = TicketAdapter::class.java.simpleName

class TicketAdapter(private val cards: List<Fields?>) : RecyclerView.Adapter<TicketAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketAdapter.ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_ticket, parent, false)
        Log.d(TAG, "onCreateViewHolder: create view")
        return ViewHolder(view)
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(ticket: Fields) = with(itemView) {
            Log.d(TAG, "ViewHolder: init")

            itemView.findViewById<ImageView>(R.id.ic_card).setImageResource(when (ticket.typeTitre) {
                Subscription.VLILLE.toString() -> R.drawable.ic_vlille
                else -> R.drawable.ic_pass_pass_card
            })
            itemView.findViewById<TextView>(R.id.rangeValue).text = ticket.libelle as String?
            itemView.findViewById<TextView>(R.id.priceValue).text =
                resources.getString(R.string.price_format, ticket.prix)
        }
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Log.d(TAG, "onBindViewHolder: bind view")
        cards[position]?.let { holder.bind(it) }
    }

    override fun getItemCount(): Int = cards.size

}