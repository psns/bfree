package com.psns.android.bfree.map

import android.content.Intent
import android.content.res.Configuration
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.psns.android.bfree.R
import com.psns.android.bfree.melapi.data.Fields
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle


private val TAG = BottomStationModal::class.java.simpleName

class BottomStationModal(private val station: Fields, private val location: Location) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.modal_bottom_station, container, false)
        Log.d(TAG, "onCreateView")
        Log.d(TAG, "station: $station")

        val stationLocation = Location("")
        stationLocation.latitude = station.geo?.get(0)?.toDouble() ?: 0.0
        stationLocation.longitude = station.geo?.get(1)?.toDouble() ?: 0.0


        // Set stations information
        view.findViewById<TextView>(R.id.modal_title).text = station.nom
        view.findViewById<TextView>(R.id.modalBikeValue).text = station.nbvelosdispo.toString()
        view.findViewById<TextView>(R.id.modalPlaceValue).text = station.nbplacesdispo.toString()
        view.findViewById<TextView>(R.id.modalDistanceValue).text =
            resources.getString(R.string.distance_format, stationLocation.distanceTo(location).toInt())

        val dateTime = LocalDateTime.parse(station.datemiseajour, DateTimeFormatter.ISO_ZONED_DATE_TIME)

        Log.d(TAG, "dateTime: $dateTime")

        view.findViewById<TextView>(R.id.modalUpdateTimeValue).text =
            dateTime.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM))

        Log.d(TAG, "theme : ${resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK}")

        val theme = context?.theme?.obtainStyledAttributes(R.styleable.ViewStyle)

        if (theme != null) {
            view.findViewById<ImageView>(R.id.tpe_image).setImageResource(when (station.type) {
                "SANS TPE" -> theme.getResourceId(R.styleable.ViewStyle_iconWithoutTpe, 0)
                else -> theme.getResourceId(R.styleable.ViewStyle_iconTpe, 0)
            })
        }

        // Open Google Maps on click
        view.findViewById<ImageView>(R.id.buttonMaps).setOnClickListener {
            val uri =
                "https://maps.google.com/maps?saddr=${location.latitude},${location.longitude}&daddr=${stationLocation.latitude},${stationLocation.longitude}"
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent.setPackage("com.google.android.apps.maps")
            startActivity(intent)
        }

        return view
    }

}