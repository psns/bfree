package com.psns.android.bfree.melapi.data

import com.google.gson.annotations.SerializedName

data class Record (
    @SerializedName("datasetid")
    val datasetId: String = "",
    @SerializedName("recordid")
    val recordId: String = "",
    @SerializedName("fields")
    val fields: Fields? = null,
    @SerializedName("geometry")
    val geometry: Geometry? = null,
    @SerializedName("record_timestamp")
    val recordTimestamp: String = "",
)