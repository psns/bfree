package com.psns.android.bfree.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface CardDao {

    @Query("SELECT * FROM card order by validity asc")
    fun getAll(): List<Card>

    @Query("SELECT * FROM card order by validity asc limit 1")
    fun getLastCard(): Card?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(card: Card)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(card: Card)

    @Delete()
    fun delete(card: Card)

}