package com.psns.android.bfree.account

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.psns.android.bfree.melapi.Api
import com.psns.android.bfree.melapi.Subscription
import com.psns.android.bfree.melapi.data.Fields

class AccountView : ViewModel() {

    private val _subscriptions = MutableLiveData<List<Fields?>>()

    private val _id = MutableLiveData<String>()
    private val _type = MutableLiveData<Subscription>()
    private val _range = MutableLiveData<Int>()
    private val _validity = MutableLiveData<String>()

    init {
        Api.getAllSubscriptions { records ->
            Log.d("AccountView", "getAllSubscriptions: $records")
            _subscriptions.value = records?.map { it.fields }
        }
    }

    fun setId(id: String) {
        _id.value = id
    }

    fun setType(type: Subscription) {
        _type.value = type
    }

    fun setRange(range: Int) {
        _range.value = range
    }

    fun setValidity(validity: String) {
        _validity.value = validity
    }

    fun getId(): String? {
        return _id.value
    }

    fun getType(): Subscription? {
        return _type.value
    }

    fun getRange(): Int? {
        return _range.value
    }

    fun getValidity(): String? {
        return _validity.value
    }

    fun getSubscriptions(): List<Fields?>? {
        return _subscriptions.value
    }

    override fun toString(): String {
        return "AccountView(id=${_id.value}, type=${_type.value}, range=${_range.value}, validity=${_validity.value})"
    }

}
