package com.psns.android.bfree.map

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.psns.android.bfree.R
import java.util.*


private val TAG = MapsFragment::class.java.simpleName

@SuppressLint("MissingPermission")
class MapsFragment : Fragment(), OnMapReadyCallback {

    private lateinit var mapViewModel: MapViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var map: GoogleMap
    private lateinit var mapView: View
    private var autocompleteSupportFragment: AutocompleteSupportFragment? = null

    private var lastLocation: Location? = null

    private var mLocationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location = locationResult.lastLocation
            lastLocation = location
            if (mapViewModel.isResetLocation()) setLocation(location)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View? {
        mapViewModel = ViewModelProvider(activity!!)[MapViewModel::class.java]
        Log.d(TAG, "onCreateView: ${mapViewModel.getPermission()}")

        val ai: ApplicationInfo = requireContext().packageManager.getApplicationInfo(requireContext().packageName,
            PackageManager.GET_META_DATA)
        val value = ai.metaData["com.google.android.geo.API_KEY"]

        if (!Places.isInitialized()) {
            Places.initialize(requireContext(), value.toString(), Locale.FRANCE)
        }

        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        if (mapFragment != null) {
            mapView = mapFragment.view!!
        }

        Log.d(TAG, "onViewCreated: ${mapViewModel.getPermission()}")

        fusedLocationClient.requestLocationUpdates(MapConfig.mLocationRequest,
            mLocationCallback,
            Looper.getMainLooper())

        // Initialize Autocomplete Fragments
        // from the main activity layout file
        autocompleteSupportFragment =
            childFragmentManager.findFragmentById(R.id.autocomplete_fragment) as AutocompleteSupportFragment?
        autocompleteSupportFragment?.setCountries("FR", "BE")
        autocompleteSupportFragment?.setPlaceFields(listOf(Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG))

        val etPlace = autocompleteSupportFragment?.view?.findViewById<EditText>(R.id.places_autocomplete_search_input)
        etPlace?.setTextColor(when (getTheme()) {
            Configuration.UI_MODE_NIGHT_YES -> Color.WHITE
            else -> Color.BLACK
        })

        autocompleteSupportFragment?.view?.findViewById<View>(R.id.places_autocomplete_clear_button)
            ?.setOnClickListener {
                autocompleteSupportFragment?.setText("")
                mapViewModel.getMarker()?.remove()
                mapViewModel.setMarker(null)
            }

        // Display the fetched information after clicking on one of the options
        autocompleteSupportFragment?.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                // Information about the place
                val name = place.name
                val address = place.address
                val latLng = place.latLng
                Log.d(TAG, "onPlaceSelected: $name, $address, $latLng")
                if (latLng == null) {
                    return
                }
                val markerOption = MarkerOptions().position(latLng).title(name).snippet(address)
                    .icon(bitmapDescriptorFromVector(requireContext(), when (getTheme()) {
                        Configuration.UI_MODE_NIGHT_YES -> R.drawable.ic_map_marker_white
                        else -> R.drawable.ic_map_marker
                    }))
                mapViewModel.setMarker(map.addMarker(markerOption))
                // Move the camera to the selected place
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mapViewModel.getZoom()))
            }

            override fun onError(status: Status) {
                Log.d(TAG, "onError: $status")
                Toast.makeText(context, "Some error occurred", Toast.LENGTH_SHORT).show()
            }
        })

        mapViewModel.getUpdateTime().observe(viewLifecycleOwner) {
            Log.d(TAG, "onMapReady: updateTime: $it")
            if (it != null) {
                view.findViewById<TextView>(R.id.updateTime)?.text = it
            }
        }

    }

    /**
     * Sets the map's camera position to the device's current location.
     */
    private fun setLocation(location: Location) {
        Log.d(TAG, "setLocation: $location")
        mapViewModel.setZoom(map.cameraPosition.zoom)

        val latLng = LatLng(location.latitude, location.longitude)
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(mapViewModel.getZoom()).build()
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.setMinZoomPreference(MapConfig.MIN_ZOOM)
        map.setMaxZoomPreference(MapConfig.MAX_ZOOM)
        map.setOnMyLocationButtonClickListener {
            Log.i(TAG, "onMyLocationButtonClick: reset location")
            mapViewModel.resetLocation(!mapViewModel.isResetLocation())
            if (mapViewModel.isResetLocation()) Toast.makeText(requireContext(),
                "Fix map to current location",
                Toast.LENGTH_SHORT).show()
            map.uiSettings.setAllGesturesEnabled(!mapViewModel.isResetLocation())
            changeResetButtonColor(if (mapViewModel.isResetLocation()) R.color.quantum_googblue else R.color.transparent)
            false
        }

        map.setOnMarkerClickListener { marker ->
            // move to marker
            map.moveCamera(CameraUpdateFactory.newLatLng(marker.position))

            // create the bottom sheet dialog
            val station = mapViewModel.getStations()?.find { it.nom == marker.title }
            Log.d(TAG, "MarkerClick: ${marker.title} : ${station?.etat} : ${station?.datemiseajour}")
            if (station != null) {
                BottomStationModal(station, lastLocation!!).show(childFragmentManager, "BottomStationModal")
            } else {
                marker.showInfoWindow()
                BottomListStationModal(map, marker.position).show(childFragmentManager, "BottomListStationModal")
            }
            true
        }

        if (getTheme() == Configuration.UI_MODE_NIGHT_YES) map.setMapStyle(MapStyleOptions.loadRawResourceStyle(
            requireContext(),
            R.raw.map_theme))

        mapViewModel.getMarkersOptions().observe(viewLifecycleOwner) {
            Log.d(TAG, "onMapReady: markers: ${it.size}")
            it.forEach { marker ->
                if (marker != null) {
                    map.addMarker(marker)
                }
            }
        }

        mapViewModel.getPermissionLive().observe(viewLifecycleOwner){
            map.isMyLocationEnabled = mapViewModel.getPermission()
            getDeviceLocation()
        }
    }

    /**
     * Get the best and most recent location of the device, which may be null in rare
     * cases when a location is not available.
     */
    private fun getDeviceLocation() {
        try {
            Log.i(TAG, "getDeviceLocation: getting location ${mapViewModel.getPermission()}")
            if (!mapViewModel.getPermission()) {
                return
            }
            val locationResult = fusedLocationClient.lastLocation
            locationResult.addOnCompleteListener {
                if (it.isSuccessful) {
                    // Set the map's camera position to the current location of the device.
                    val location = it.result
                    Log.d(TAG, "getDeviceLocation: $location")
                    if (location != null) {
                        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude,
                            location.longitude), MapConfig.DEFAULT_ZOOM))
                        lastLocation = location
                    }
                } else {
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(MapConfig.defaultLocation,
                        MapConfig.DEFAULT_ZOOM))
                    map.uiSettings.isMyLocationButtonEnabled = false
                }
            }

        } catch (e: SecurityException) {
            e.message?.let { Log.e("Exception: %s", it) }
        }
    }

    @SuppressLint("ResourceType")
    private fun changeResetButtonColor(color: Int) {
        mapView.findViewById<ImageView>(2).setBackgroundColor(ContextCompat.getColor(requireContext(), color))
    }

    /**
     * Get the current theme
     */
    private fun getTheme(): Int {
        return requireContext().resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
    }

    /**
     * Create icon from drawable resource
     */
    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap =
            Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

}