package com.psns.android.bfree.map

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.psns.android.bfree.melapi.Api
import com.psns.android.bfree.melapi.data.Fields
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private val TAG = MapViewModel::class.java.simpleName

class MapViewModel : ViewModel() {

    private val _mapMarkers = MutableLiveData<List<MarkerOptions?>>()
    private val _marker = MutableLiveData<Marker?>()

    private val _resetLocation = MutableLiveData(false)
    private val _permission = MutableLiveData(false)
    private val _zoom = MutableLiveData(MapConfig.DEFAULT_ZOOM)
    private val _updateTime = MutableLiveData("")

    private val _stations = MutableLiveData<List<Fields>>()
    private val _surroundStations = MutableLiveData<List<Fields>>()

    init {
        Log.d(TAG, "MapView created")

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                while (true) {
                    Api.getAllStations(success = { stations ->
                        if (stations == null || stations.isEmpty()) {
                            return@getAllStations
                        }

                        val updateDate =
                            stations.stream().map { it.fields?.datemiseajour!! }.max(String::compareTo).get()

                        Log.d(TAG, "displayMarkers: ${stations.size} $updateDate")
                        setStations(stations.map { it.fields!! })
                        _updateTime.postValue(updateDate)
                        _mapMarkers.postValue(stations.map {
                            val station = it.fields
                            if (station == null || station.geo?.size != 2) {
                                return@map null
                            }
                            val geo = LatLng(station.geo[0].toDouble(), station.geo[1].toDouble())

                            val color = if (station.nbvelosdispo!! == 0) BitmapDescriptorFactory.HUE_RED
                            else if (station.nbplacesdispo!! == 0) BitmapDescriptorFactory.HUE_BLUE
                            else BitmapDescriptorFactory.HUE_GREEN

                            return@map MarkerOptions().position(geo).title(station.nom)
                                .icon(BitmapDescriptorFactory.defaultMarker(color))
                        } as ArrayList<MarkerOptions?>)
                    })
                    delay(MapConfig.REFRESH_TIME)
                }
            }
        }
    }

    fun isResetLocation(): Boolean {
        return _resetLocation.value as Boolean
    }

    fun resetLocation(value: Boolean) {
        _resetLocation.postValue(value)
    }

    fun getZoom(): Float {
        return _zoom.value as Float
    }

    fun getMarkersOptions(): MutableLiveData<List<MarkerOptions?>> {
        return _mapMarkers
    }

    fun getMarker(): Marker? {
        return _marker.value
    }

    fun setMarker(marker: Marker?) {
        _marker.postValue(marker)
    }

    fun setZoom(zoom: Float) {
        this._zoom.postValue(zoom)
    }

    fun getPermissionLive(): MutableLiveData<Boolean> {
        return _permission
    }

    fun getPermission(): Boolean {
        return _permission.value as Boolean
    }

    fun setPermission(permission: Boolean) {
        Log.d("MapView", "setPermission: $permission")
        this._permission.postValue(permission)
    }

    fun getStations(): List<Fields>? {
        return _stations.value
    }

    private fun setStations(stations: List<Fields>) {
        this._stations.postValue(stations)
    }

    fun getSurroundStations(): MutableLiveData<List<Fields>> {
        return _surroundStations
    }

    fun setSurroundStations(location: LatLng, distance: Int = 500) {
        Api.getSurroundingStations(location, distance) { stations ->
            if (stations == null || stations.isEmpty()) this._surroundStations.postValue(emptyList())
            else this._surroundStations.postValue(stations.map { it.fields!! })
        }
    }

    fun getUpdateTime(): MutableLiveData<String> {
        return _updateTime
    }

    override fun onCleared() {
        super.onCleared()
        Log.d(TAG, "GameViewModel destroyed!")
    }

}