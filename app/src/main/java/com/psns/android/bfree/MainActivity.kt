package com.psns.android.bfree

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.psns.android.bfree.map.MapViewModel

private val TAG = MainActivity::class.java.simpleName

class MainActivity : AppCompatActivity() {

    private lateinit var mapViewModel: MapViewModel

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        mapViewModel = ViewModelProvider(this)[MapViewModel::class.java]

        enableMyLocation()
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    @SuppressLint("MissingPermission")
    private fun enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            Log.d(TAG, "enableMyLocation: permission granted")
            mapViewModel.setPermission(true)
        } else {
            Log.d(TAG, "enableMyLocation: permission not granted")
            mapViewModel.setPermission(false)
            // Permission to access the location is missing. Show rationale and request permission
            PermissionUtils.requestPermission(this,
                PermissionUtils.LOCATION_PERMISSION_REQUEST_CODE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                true)
        }
    }

    /**
     * Runs when a request for permission to access fine location is received.
     * @param requestCode The request code passed in [.requestPermission].
     * @param permissions The permissions the user granted.
     * @param grantResults The grant results for the corresponding permissions
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode != PermissionUtils.LOCATION_PERMISSION_REQUEST_CODE) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            return
        }
        if (PermissionUtils.isPermissionGranted(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            Log.i(TAG, "Location permission granted")
            mapViewModel.setPermission(true)
            this.finish()
            startActivity(this.intent)
        } else {
            // Permission was denied. Display an error message
            // Display the missing permission error dialog when the fragments resume.
            requestLocationPermission(PermissionUtils.LOCATION_PERMISSION_REQUEST_CODE)
            mapViewModel.setPermission(false)
        }
    }

    /**
     * Requests the fine location permission. If a rationale with an additional explanation should
     * be shown to the user, displays a dialog that triggers the request.
     */
    private fun requestLocationPermission(requestCode: Int) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Display a dialog with rationale.
            PermissionUtils.RationaleDialog.newInstance(requestCode, false).show(supportFragmentManager, "dialog")
        }
    }

}
