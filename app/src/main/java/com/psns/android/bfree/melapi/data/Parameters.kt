package com.psns.android.bfree.melapi.data

import com.google.gson.annotations.SerializedName

data class Parameters(
    @SerializedName("dataset")
    val dataset: String? = null,
    @SerializedName("q")
    val q: String? = null,
    @SerializedName("lang")
    val lang: String? = null,
    @SerializedName("rows")
    val rows: Int? = null,
    @SerializedName("start")
    val start: Int = 0,
    @SerializedName("sort")
    val sort: List<String>? = null,
    @SerializedName("facet")
    val facet: List<String>? = null,
    @SerializedName("refine")
    val refine: Map<String, String>? = null,
    @SerializedName("exclude")
    val exclude: Map<String, String>? = null,
    @SerializedName("geofilter.distance")
    val geofilterDistance: List<String>? = null,
    @SerializedName("geofilter.polygon")
    val geofilterPolygon: String? = null,
    @SerializedName("timezone")
    val timezone: String? = null,
    @SerializedName("fields")
    val fields: List<String>? = null,
)
