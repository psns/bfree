package com.psns.android.bfree.melapi.data

import com.google.gson.annotations.SerializedName

data class Geometry(
    @SerializedName("type")
    val type: String,
    @SerializedName("coordinates")
    val coordinates: List<String>
)
