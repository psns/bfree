package com.psns.android.bfree.account

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.psns.android.bfree.R
import com.psns.android.bfree.db.AppDatabase
import com.psns.android.bfree.db.Card
import com.psns.android.bfree.melapi.Subscription
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

private val TAG = CardAdapter::class.java.simpleName

class CardAdapter(
    private val db: AppDatabase,
    private val cards: ArrayList<Card>,
    private var listener: OnItemClickListener?,
) :
    RecyclerView.Adapter<CardAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    private fun removeCard(position: Int) {
        val card = cards[position]
        db.cardDao().delete(card)
        cards.remove(card)
        notifyItemRemoved(position)
    }

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val dateFormat = "yyyy/MM/dd"

        fun bind(card: Card) = with(itemView) {
            Log.d(TAG, "ViewHolder: init")

            val layout = itemView.findViewById<LinearLayout>(R.id.valuesLayout)
            val icon = itemView.findViewById<ImageView>(R.id.ic_card)

            val date = OffsetDateTime.parse(card.validity, DateTimeFormatter.ISO_DATE_TIME)

            itemView.findViewById<TextView>(R.id.idValue).text = card.id
            itemView.findViewById<TextView>(R.id.validityValue).text =
                date.format(DateTimeFormatter.ofPattern(dateFormat))
            itemView.findViewById<TextView>(R.id.typeValue).text = card.type.toString()
            itemView.findViewById<TextView>(R.id.rangeValue).text = card.range

            layout.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    listener?.onItemClick(position)
                }
            }

            icon.setImageResource(when (card.type) {
                Subscription.VLILLE -> R.drawable.ic_vlille
                else -> R.drawable.ic_pass_pass_card
            })

            itemView.findViewById<Button>(R.id.deleteButton).setOnClickListener {
                removeCard(adapterPosition)
            }

            icon.setOnClickListener {
                val text = resources.getString(R.string.validity_format,
                    date.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)))
                Toast.makeText(context, text, Toast.LENGTH_LONG).show()
            }

            val now = OffsetDateTime.now()
            if (date.isBefore(now)) {
                itemView.findViewById<LinearLayout>(R.id.validityLayout).setBackgroundColor(Color.RED)
            }
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.fragment_card, viewGroup, false)
        Log.d(TAG, "onCreateViewHolder: create view")
        Log.d(TAG, "onCreateViewHolder: cards size: ${cards.size}")
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        Log.d(TAG, "onBindViewHolder: bind view")
        viewHolder.bind(cards[position])
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = cards.size

}