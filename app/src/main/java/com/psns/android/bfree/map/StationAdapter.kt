package com.psns.android.bfree.map

import android.location.Location
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.findFragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.psns.android.bfree.R
import com.psns.android.bfree.melapi.data.Fields

private const val TAG = "StationAdapter"

class StationAdapter(
    private val viewModel: MapViewModel,
    private val map: GoogleMap,
    private val stations: List<Fields>,
    private val location: LatLng,
) : RecyclerView.Adapter<StationAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(station: Fields) = with(itemView) {
            Log.d(TAG, "ViewHolder: init")

            val stationLocation = Location("")
            stationLocation.latitude = station.geo?.get(0)?.toDouble() ?: 0.0
            stationLocation.longitude = station.geo?.get(1)?.toDouble() ?: 0.0

            val positionLocation = Location("")
            positionLocation.latitude = location.latitude
            positionLocation.longitude = location.longitude

            itemView.findViewById<ImageView>(R.id.iconMaps).setOnClickListener {
                val latLng = LatLng(stationLocation.latitude, stationLocation.longitude)
                val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18F).build()
                map.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
                viewModel.getMarker()?.hideInfoWindow()
                findFragment<BottomListStationModal>().dismiss()
            }

            // Set stations information
            itemView.findViewById<TextView>(R.id.modalNameValue).text = station.nom
            itemView.findViewById<TextView>(R.id.modalBikeValue).text = station.nbvelosdispo.toString()
            itemView.findViewById<TextView>(R.id.modalPlaceValue).text = station.nbplacesdispo.toString()
            itemView.findViewById<TextView>(R.id.modalDistanceValue).text =
                resources.getString(R.string.distance_format, stationLocation.distanceTo(positionLocation).toInt())
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.fragment_station, viewGroup, false)
        Log.d(TAG, "onCreateViewHolder: create view")
        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        Log.d(TAG, "onBindViewHolder: bind view")
        viewHolder.bind(stations[position])
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = stations.size

}