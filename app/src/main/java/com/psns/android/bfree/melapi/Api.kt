package com.psns.android.bfree.melapi

import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.psns.android.bfree.melapi.data.Parameters
import com.psns.android.bfree.melapi.data.Record
import com.psns.android.bfree.melapi.data.Response
import com.psns.android.bfree.melapi.data.Fields
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.LocalDate

const val BASE_API_URL =
    "https://opendata.lillemetropole.fr/api/records/1.0/search/"
const val DEFAULT_ROWS = 10
const val MAX_DEFAULT_ROWS = -1

val TAG = Api::class.java.simpleName

object Api {
    @JvmStatic
    private fun fetchAPI(params: Parameters = Parameters(), success: (Response) -> Unit) {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_API_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val service = retrofit.create(MelAPIService::class.java)
        val call = service.fetch(
            dataset = params.dataset,
            q = params.q,
            lang = params.lang,
            rows = params.rows,
            start = params.start,
            sort = params.sort,
            facet = params.facet,
            refine = params.refine,
            exclude = params.exclude,
            geofilterDistance = params.geofilterDistance,
            geofilterPolygon = params.geofilterPolygon,
            timezone = params.timezone,
            fields = params.fields
        )

        call.request().url().toString().let {
            Log.d(TAG, it)
        }

        call.enqueue(object : Callback<Response> {
            override fun onResponse(call: Call<Response>, response: retrofit2.Response<Response>) {
                if (response.code() == 200) {
                    success(response.body()!!)
                }
            }
            override fun onFailure(call: Call<Response>, t: Throwable) {
                    Log.v(TAG, "Failed")
                    Log.v(TAG, t.localizedMessage!!)
            }
        })
    }

    /**
     * Return the first one hundred station near the given point within the given distance.
     */
    @JvmStatic
    fun getSurroundingStations(location: LatLng, distance: Int, success: (List<Record>?) -> Unit) {
        fetchAPI(
            Parameters(
                dataset = DATASET_VLILLE,
                geofilterDistance = arrayListOf("${location.latitude}, ${location.longitude}, $distance"),
                rows = DEFAULT_ROWS,
                q = "etat=\"EN SERVICE\"" // datemiseajour>=${LocalDate.now().minusDays(1)}
            ),
            success = { response -> success(response.records) }
        )
    }

    @JvmStatic
    fun getAllStations(success: (List<Record>?) -> Unit) {
        fetchAPI(
            Parameters(
                dataset = DATASET_VLILLE,
                rows = MAX_DEFAULT_ROWS,
                q = "etat=\"EN SERVICE\"" // datemiseajour>=${LocalDate.now().minusDays(1)}
            ),
            success = { response -> success(response.records) }
        )
    }

    @JvmStatic
    fun getAllSubscriptions(success: (List<Record>?) -> Unit) {
        fetchAPI(
            Parameters(
                dataset = DATASET_PRICES,
                rows = MAX_DEFAULT_ROWS,
            ),
            success = { response -> success(response.records) }
        )
    }

    @JvmStatic
    fun searchStations(name: String, success: (List<Record>?) -> Unit) {
        val searchName = name.uppercase()
        fetchAPI(
            Parameters(
                dataset = DATASET_VLILLE,
                rows = MAX_DEFAULT_ROWS,
                q = "datemiseajour>=${LocalDate.now().minusDays(1)}",
                fields = arrayListOf("nom")
            ),
            success = { response ->
                val filteredResult = arrayListOf<Record>()

                if (response.records != null) {
                    response.records.forEach { station: Record ->
                        if (station.fields?.nom?.contains(searchName)!!) {
                            filteredResult.add(station)
                        }
                    }
                }

                success(filteredResult)
            }
        )
    }
}