package com.psns.android.bfree.tickets

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.psns.android.bfree.melapi.Api
import com.psns.android.bfree.melapi.Subscription
import com.psns.android.bfree.melapi.data.Fields

class TicketsViewModel : ViewModel() {

    private val _tickets = MutableLiveData<List<Fields?>>()
    private val _type = MutableLiveData<String>(Subscription.VLILLE.toString())

    init {
        Api.getAllSubscriptions { records ->
            Log.d("TicketsViewModel", "getAllSubscriptions: $records")
            _tickets.value = records?.map { it.fields }
        }
    }

    fun getTickets() = _tickets.value?.filter { it?.typeTitre == _type.value }?.sortedByDescending { it?.prix }

    fun getType() = _type

    fun setType(type: String) {
        _type.value = type
    }

}