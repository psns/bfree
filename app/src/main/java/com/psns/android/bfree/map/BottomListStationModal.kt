package com.psns.android.bfree.map

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.psns.android.bfree.R
import com.psns.android.bfree.melapi.data.Fields


private val TAG = BottomListStationModal::class.java.simpleName

class BottomListStationModal(private val map: GoogleMap, private val location: LatLng) : BottomSheetDialogFragment() {

    private lateinit var mapViewModel: MapViewModel

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        mapViewModel = ViewModelProvider(activity!!)[MapViewModel::class.java]

        val view = inflater.inflate(R.layout.modal_bottom_list_station, container, false)
        Log.d(TAG, "onCreateView")

        mapViewModel.setSurroundStations(location, 500)
        mapViewModel.getSurroundStations().observe(viewLifecycleOwner) {
            Log.d(TAG, "surround stations: ${it.size}")
            val recyclerView = view.findViewById<RecyclerView>(R.id.stationsListLayout)
            recyclerView.layoutManager = LinearLayoutManager(context);
            recyclerView.adapter = StationAdapter(mapViewModel, map, it, location)
        }

        return view
    }

}